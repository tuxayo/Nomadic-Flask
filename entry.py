#!/bin/python
import glob
import markdown
import os
import codecs
from flask import Flask
from flask import render_template
from flask import Markup
from flask import json
from flask import send_file

app = Flask(__name__)

# Default config
config = dict(
  blog_name = "Nomadic-Flask",
  blog_subtitle = "Subtitle here",
  static_dir = "static",
  index_file = "index.json",
  img_location = "imgs",
  markdown_extensions = ["markdown.extensions.meta"],
  post_num_per_page = 5,
  debug_mode = True,
  public_mode = False,
  port= 80
)


# Utilities

def parse_config(filename):
    try:
        data = json.load(codecs.open('config.json', mode='r', encoding='utf-8'))
        for key, val in enumerate(data):
            config[val] = data[val]
    except IOError, ValueError:
        app.logger.error('Parsing error')
    print("Done")

def get_first_and_last_post_from_page(page_num, posts_num):
    return max((page_num-1) * config['post_num_per_page'], 0), min(page_num * config['post_num_per_page'], posts_num)

def get_meta_data(meta):
    title, icon, date, tags = 'No Title', '', 'No date', ['Untagged']
    if( 'title' in meta ):
        title = meta['title'][0]
    if( 'date' in meta ):
        date = meta['date'][0]
    if( 'icon' in meta ):
        icon = meta['icon'][0]
    if( 'tags' in meta ):
        tags = [x.strip() for x in meta['tags'][0].split(',')]
    return title, date, icon, tags


def parse_index(index_data, current_page):
    first_article, last_article = get_first_and_last_post_from_page(current_page, len(index_data))
    result = []
    md = markdown.Markdown(extensions = config['markdown_extensions'])
    article_strip = index_data[first_article:last_article]

    if not article_strip:
        return result
    for entry in article_strip:
        data = codecs.open(os.path.join(config['static_dir'], 'text', entry), mode='r', encoding='utf-8').read()
        fileContent = Markup(md.convert(data)).striptags()
        fileName, fileExtension = os.path.splitext(os.path.basename(entry))
        title, date, icon, tags = get_meta_data(md.Meta)
        result.append(dict(title=title, date=date, content=fileContent, link='/post/%s' % fileName, icon=icon, tags=tags, more=True))
        md.reset()
    return result

# Routes

@app.route("/imgs/<path:picture>")
def deliver_picture(picture):
    return send_file(os.path.join(config['static_dir'], config['img_location'], picture))

@app.route("/")
@app.route("/page/<int:page>")
def root(page = 1):
    try:
        index_data = json.load(codecs.open(os.path.join(config['static_dir'], config['index_file']), mode='r', encoding='utf-8'))
    except IOError:
        return render_template('error.html', blog_title=config['blog_name'], blog_subtitle=config['blog_subtitle'], head_title=config['blog_name']+' - Oops!', content=dict(text='Coudln\'t find index.'))
    except ValueError:
        return render_template('error.html', blog_title=config['blog_name'], blog_subtitle=config['blog_subtitle'], head_title=config['blog_name']+' - Oops!', content=dict(text='Coudln\'t parse index.'))

    entries = parse_index(index_data, page)

    prev, next = '', ''
    if(page < len(index_data) / config['post_num_per_page']):
        next = page+1
    if(page > 2):
        prev = page -1

    content = dict(entries=entries, prev=prev, next = next)
    return render_template('index.html', blog_title=config['blog_name'], blog_subtitle=config['blog_subtitle'], head_title=config['blog_name'], content=content)


@app.route('/post/<name>')
def post(name):
    data = codecs.open(os.path.join(config['static_dir'], 'text', '%s.md' % name), mode='r', encoding='utf-8').read()
    md = markdown.Markdown(extensions = config['markdown_extensions'])

    text = Markup(md.convert(data))
    title, date, icon, tags = get_meta_data(md.Meta)
    content = dict(title=title, date=date, text=text, tags=tags, icon=icon)
    return render_template('post.html', blog_title=config['blog_name'], blog_subtitle=config['blog_subtitle'], head_title=config['blog_name']+' - '+title, content=content)


if __name__ == "__main__":
    parse_config('config.json')
    if(config['public_mode']):
        app.run(debug=config['debug_mode'], host='0.0.0.0', port=config['port'])
    app.run(debug=config['debug_mode'], port=config['port'])
