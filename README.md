# Nomadic Flask
## DB-less blog framework

Nomadic Flask is a little project I wanted to test [Flask] in a superficial way while having a usable result : a blog framework.

## Features

- DB-less blog management : no need of MariaDB/MySQL, SQLLite, some NoSQL base or whatever you use.
- Markdown parsing : Actually in the works for some details (like pictures), but you can already create all text blog posts.

## How to use

### Installation
1. Install python and pip
2. `$ pip install Flask`
3. Clone this repo in a folder.
4. In the repo's folder, run entry.py `$ python entry.py` in a terminal. If everything works, it should display this line ` * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)` and should wait for clients to connect, the web server is ready.

### Managing posts

In `static` folder lies the folders which makes the style and the content of the blog.
- `css` stores the stylesheets
- `fonts` stores Bootstrap's fonts
- `js` stores Bootstrap's script

- `text` will store your blog posts. Each post will be contained in a Markdown file (`name.md`) where  `name` will be the link's name `http://address/post/name` will open `static/text/name.md`.
- `templates` stores the HTML templates for Flask to allow him to build the page as you want. Here I used a Bootply [template] I quickly chose to test the framework and the template system.
- `ìndex.json` is the post index. THe blog will follow them in the descendent order. The first entry will be the most recent post in the blog, the second will follow the first and so on.

Note : Don't mind the already present blog post, these are only existing to check if everything worked  (or at least more or less)

## Hacking the framework

So, it seems you want to tweak with the framework, isn't it? First you should read [Flask]'s documentation to know what does what. Let's see how the blog works part by part

### Entry script

The script run by Python processes the arguments sent by the client and will manage the IO with the client and the files. Root (`//`) and `/page` will yield the blog's index pages, `/post` route will yield the full text of a blog post in its page.

### JSON API(?) (WIP and pondering if it'd be useful)

### Template

The `template` folder stores all the HTML files which makes the bricks to build the pages.
- `skeleton.html` is the common parts of every page, from the index to the error pages.
- `index.html` is called to build the index.
- `post.html` is called to build the one post view.
- `modules.html` contains multiples functions used in multiple files, like `blog_post` macro.


## Resources used

- [Bootstrap Zero's blog template][template], used just to have a quick nice result, MIT License.
- This template uses [Bootstrap][bootstrap]


[Flask]: http://flask.pocoo.org/
[template]: http://www.bootstrapzero.com/bootstrap-template/blog
[bootstrap]: http://getbootstrap.com/

## License?

I don't know which kind of licence to use. I think I'm going to use the  MIT License.